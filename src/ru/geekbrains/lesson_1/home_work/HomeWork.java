package ru.geekbrains.lesson_1.home_work;

public class HomeWork {

    /**
     * Задание 1.
     *
     * Создать пустой проект в IntelliJ IDEA и прописать метод main().
     */
    public static void main(String[] args) {}

    /**
     * Задание 2.
     *
     * Создать переменные всех пройденных типов данных, и инициализировать их значения.
     */
    private static void javaTypes()
    {
        // Примитивные типы
        byte b = 127;
        short sh = 32767;
        int i = 2147483647;
        long l = 99735628203303L;

        float f = 0.4f;
        double d = 5.54;

        char c = 's';

        boolean bool = false;

        // Ссылочные типы
        String str = new String("a string");
    }

    /**
     * Задание 3.
     *
     * Написать метод вычисляющий выражение a * (b + (c / d)) и возвращающий результат,
     * где a, b, c, d – входные параметры этого метода.
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    private static int calculate(int a, int b, int c, int d) {
        return a * (b + c / d);
    }

    /**
     * Задание 4.
     *
     * Написать метод, принимающий на вход два числа, и проверяющий что их сумма лежит в пределах
     * от 10 до 20 (включительно), если да – вернуть true, в противном случае – false.
     *
     * @param a
     * @param b
     * @return
     */
    private static boolean isSumInRangeFrom10to20(int a, int b) {
        int sum = a + b;
        return (sum >= 10 & sum <= 20);
    }

    /**
     * Задание 5.
     *
     * Написать метод, которому в качестве параметра передается целое число, метод должен напечатать в консоль
     * положительное ли число передали, или отрицательное.
     *
     * Замечание: ноль считаем положительным числом.
     *
     * @param number
     */
    private static void printIsNumberPositiveOrNegative(int number) {
        if (isNegativeNumber(number)) {
            System.out.println("Число " + number + " является отрицательным.");
            return;
        }

        System.out.println("Число " + number + " является положительным.");
    }

    /**
     * Задание 6.
     *
     * Написать метод, которому в качестве параметра передается целое число, метод должен вернуть true,
     * если число отрицательное.
     *
     * @param number
     * @return
     */
    private static boolean isNegativeNumber(int number) {
        return number < 0;
    }

    /**
     * Задание 7.
     *
     * Написать метод, которому в качестве параметра передается строка, обозначающая имя, метод должен вывести в
     * консоль сообщение «Привет, указанное_имя!».
     *
     * @param name
     */
    private static void printGreeting(String name) {
        System.out.println("Привет, " + name + "!");
    }

    /**
     * Задание 8. *
     *
     * Написать метод, который определяет является ли год високосным, и выводит сообщение в консоль.
     * Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й – високосный.
     *
     * @param year
     * @throws IllegalArgumentException
     */
    private static void isLeapYear(int year) throws IllegalArgumentException {
        if (isNegativeNumber(year)) {
            throw new IllegalArgumentException(
                "Значение параметра 'year' должно быть положительным целым числом, передано year=" + year
            );
        }

        if ((year % 4 == 0 & year % 100 != 0) | year % 400 == 0) {
            System.out.println("Год " + year + " является высокосным.");
            return;
        }

        System.out.println("Год " + year + " не является высокосным.");
    }
}
